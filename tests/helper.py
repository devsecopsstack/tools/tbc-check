from tbc_check import checker


def make_var_fixture(name: str, description: str, default: str, type: checker.TbcVarType):
    input_name = name.lower().replace("_","-")
    input_type = None
    if type == checker.TbcVarType.boolean:
        input_type = "boolean"
    elif type == checker.TbcVarType.text:
        input_type = "string"

    return {
        "tbc_var": checker.TbcVar(
            name=name,
            description=description,
            type=type,
            default=default
        ),
        "var_prefix": "",
        "tpl_spec": {
            "spec": {
                "inputs" : {
                    input_name: {
                        "description": description,
                        "type": input_type,
                        "default": default
                    }
                }
            }
        },
        "tpl_body": {
            "variables": {
                name: f"$[[ inputs.{input_name} ]]"
            }
        },
        "root_kicker": None,
        "doc_vars": [checker.DocVar(
            var_name=name,
            input_name=input_name,
            description=description,
            default_cell=f"`{default}`",
            lock=False
        )]
    }
