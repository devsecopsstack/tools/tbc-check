import pytest

from tbc_check import checker


def test_check_job_good_jobs(capfd: pytest.CaptureFixture[str]):
    tpl_body = {
        "workflow": None,
        "variables": None,
        "stages": None,
        ".hidden": None,
        "prefix-job1": {},
        "prefix-job2": {},
    }

    res = checker._check_job("prefix-job1", tpl_body, "prefix")
    out, err = capfd.readouterr()
    assert out == ("")


def test_check_job_bad_jobs(capfd: pytest.CaptureFixture[str]):
    tpl_body = {
        "workflow": None,
        "variables": None,
        "stages": None,
        ".hidden": None,
        "bad-prefix-job1": {},
        "bad-prefix-job2": {},
    }

    res = checker._check_job("bad-prefix-job1", tpl_body, "prefix")
    out, err = capfd.readouterr()
    assert out == (
        "  \x1b[0;31m✕ job <bad-prefix-job1>: doesn't start with prefix (prefix)\x1b[0m\n"
    )
