# [1.4.0](https://gitlab.com/to-be-continuous/tools/tbc-check/compare/1.3.1...1.4.0) (2024-05-05)


### Bug Fixes

* turn single job template inheritance rule into warning (for make) ([3352dea](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/3352dea91f51c62faf4370d39d37efff8785d957))


### Features

* skip OK message if at least one warn ([741cbae](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/741cbaec7be607f74c8b4fed39f9397d86a3f8d0))

## [1.3.1](https://gitlab.com/to-be-continuous/tools/tbc-check/compare/1.3.0...1.3.1) (2024-05-05)


### Bug Fixes

* don't test job prefix rule on "pages" job (special) ([0ea7702](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/0ea770281a761b2137cca3129743f060f5d69a5c))

# [1.3.0](https://gitlab.com/to-be-continuous/tools/tbc-check/compare/1.2.0...1.3.0) (2024-05-05)


### Features

* skip partial job definition and add explicit output messages for each job ([f95a821](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/f95a8219951707122ea30f11ff03ed0fa33d91e7))

# [1.2.0](https://gitlab.com/to-be-continuous/tools/tbc-check/compare/1.1.0...1.2.0) (2024-05-05)


### Features

* implement job-prototype and inheritance check ([1effdd1](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/1effdd1e8cfeabbbccdeb7e55a76c26db680ae36)), closes [#7](https://gitlab.com/to-be-continuous/tools/tbc-check/issues/7)

# [1.1.0](https://gitlab.com/to-be-continuous/tools/tbc-check/compare/1.0.0...1.1.0) (2024-04-27)


### Bug Fixes

* pydantic parse_obj deprecation warning ([ea50ac4](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/ea50ac42e094e73700cba3e9ec497126ab9a723d))


### Features

* check container image rules ([f7f54bb](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/f7f54bba300ed786d0eb92dda70d4bfa206d5365)), closes [#5](https://gitlab.com/to-be-continuous/tools/tbc-check/issues/5) [#6](https://gitlab.com/to-be-continuous/tools/tbc-check/issues/6)
* check emtpy default can be omitted in Kicker ([d19c780](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/d19c780e5a4648b6ee0199eee19636158ceec69b)), closes [#2](https://gitlab.com/to-be-continuous/tools/tbc-check/issues/2)
* check jobs prefix rule ([06fa347](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/06fa3473493d0e2a15900bcb44344217819c6f50)), closes [#4](https://gitlab.com/to-be-continuous/tools/tbc-check/issues/4)
* check report naming conventions ([f258fa8](https://gitlab.com/to-be-continuous/tools/tbc-check/commit/f258fa877d1683382b49bb823711f71d02513c15)), closes [#3](https://gitlab.com/to-be-continuous/tools/tbc-check/issues/3)
