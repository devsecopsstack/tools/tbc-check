import argparse
import json
import re
from enum import Enum
from logging import Logger
from pathlib import Path
from typing import Any, Optional, Union

import yaml
from pydantic import BaseModel

LOGGER = Logger(__name__)


class AnsiColors:
    BLACK = "\033[0;30m"
    RED = "\033[0;31m"
    GREEN = "\033[0;32m"
    YELLOW = "\033[0;33m"
    BLUE = "\033[0;34m"
    PURPLE = "\033[0;35m"
    CYAN = "\033[0;36m"
    WHITE = "\033[0;37m"

    HGRAY = "\033[90m"
    HRED = "\033[91m"
    HGREEN = "\033[92m"
    HYELLOW = "\033[93m"
    HBLUE = "\033[94m"
    HPURPLE = "\033[95m"
    HCYAN = "\033[96m"
    HWHITE = "\033[97m"

    RESET = "\033[0m"
    BOLD = "\033[1m"
    UNDERLINE = "\033[4m"


class GlInputType(str, Enum):
    """GitLab CI/CD component input type."""

    string = "string"
    boolean = "boolean"
    number = "number"


class TbcVarType(str, Enum):
    """to-be-continuous variable type."""

    text = "text"
    boolean = "boolean"
    number = "number"
    enum = "enum"
    url = "url"

    def to_gl(self) -> GlInputType:
        if self == TbcVarType.boolean:
            return GlInputType.boolean
        if self == TbcVarType.number:
            return GlInputType.number
        return GlInputType.string


class GlInput(BaseModel):
    """GitLab input from the 'specs' section."""

    description: Optional[str] = None
    """Input description."""
    type: GlInputType = GlInputType.string
    """Input type."""
    options: Optional[list[str]] = None
    """Input options (allowed values)."""
    default: Optional[Union[str, bool, int]] = None
    """Input default."""


class TbcVar(BaseModel):
    """to-be-continuous variable from the Kicker declaration."""

    name: str
    """Variable name."""
    description: Optional[str] = None
    """Variable name."""
    type: TbcVarType = TbcVarType.text
    """Variable type."""
    values: Optional[list[str]] = None
    """Variable allowed values (for an enumerated type)."""
    default: Optional[str] = None
    """Variable default value."""
    advanced: bool = False
    """Whether the variable is advanced."""
    secret: bool = False
    """Whether the variable is a secret."""
    mandatory: bool = False
    """Whether the variable is mandatory."""

    def input_name(self, var_prefix: str) -> str:
        return (
            (
                self.name[len(var_prefix) :]
                if self.name.startswith(var_prefix)
                else self.name
            )
            .replace("_", "-")
            .lower()
        )

    def to_gl(self) -> GlInput:
        return GlInput(
            # TODO: remove Markdown formatting?
            description=self.description,
            type=self.type.to_gl(),
            default=self.gl_dflt,
            options=self.values,
        )

    @property
    def gl_dflt(self):
        if self.type == TbcVarType.boolean:
            # return bool(self.default) if self.default != None else False
            return self.default or "false"
        if self.type == TbcVarType.number:
            # return int(self.default) if self.default != None else 0
            return self.default or "0"
        return self.default or ""


class DocVar(BaseModel):
    """Variable delcaration from the documentation (README)."""

    lock: bool
    """Whether the variable show a lock icon (expected for secrets)."""
    var_name: str
    """The variable name."""
    input_name: Optional[str] = None
    """The associated input name."""
    description: str
    """The textual variable description."""
    default_cell: str
    """The textual content of the default value column."""

    def default(self, type: GlInputType) -> Optional[str]:
        """Extracts and converts the variable default value from the cell content."""
        code_expr_match = re.match(r"`([^`]*)`", self.default_cell)
        explicit_default = code_expr_match.group(1) if code_expr_match else None

        if type == GlInputType.boolean:
            # return bool(self.default) if self.default != None else False
            return explicit_default or "false"
        if type == GlInputType.number:
            # return int(self.default) if self.default != None else 0
            return explicit_default or "0"
        return explicit_default or ""


def _get_var(kicker: dict[str, any], var_name: str) -> Optional[TbcVar]:
    var = next(
        iter(
            [
                TbcVar.model_validate(var)
                for var in kicker.get("variables", [])
                if var["name"] == var_name
            ]
        ),
        None,
    )
    if var:
        return var

    # look into feature variables
    for feat in kicker.get("features", []):
        if feat.get("enable_with") == var_name:
            return TbcVar(
                name=feat.get("enable_with"),
                description=f"Enable {feat['name']}",
                type=TbcVarType.boolean,
            )
        elif feat.get("disable_with") == var_name:
            return TbcVar(
                name=feat.get("disable_with"),
                description=f"Disable {feat['name']}",
                type=TbcVarType.boolean,
            )
        var = next(
            iter(
                [
                    TbcVar.model_validate(var)
                    for var in feat.get("variables", [])
                    if var["name"] == var_name
                ]
            ),
            None,
        )
        if var:
            return var

    return None


def _check_var(
    tbc_var: TbcVar,
    var_prefix: str,
    tpl_spec: dict[str, any],
    tpl_body: dict[str, any],
    root_kicker: Optional[dict[str, any]],
    doc_vars: list[DocVar],
) -> int:
    """Check variable rules."""
    has_no_input = (
        tbc_var.secret
        or root_kicker
        and _get_var(root_kicker, tbc_var.name)
        or tbc_var.name.startswith("TBC_")
    )
    expected_input_name = tbc_var.input_name(var_prefix)
    expected_gl_input = tbc_var.to_gl()

    err_count = 0
    warn_count = 0

    # check variable declaration from Kicker
    # --------------------------------------
    # check: empty default can be omitted in kicker.json
    if tbc_var.default == "":
        print(
            f"  {AnsiColors.RED}✕ <{tbc_var.name}>: empty default shall be omitted in Kicker{AnsiColors.RESET}"
        )
        err_count += 1

    # check container image rules
    if (
        tbc_var.name.endswith("_IMAGE")
        and tbc_var.default
        and not tbc_var.default.startswith("$")
    ):
        # check: explicit image registry
        img_parts = tbc_var.default.split("/")
        if img_parts[0].find(".") < 0:
            print(
                f"  {AnsiColors.RED}✕ <{tbc_var.name}>: container images must explicitly specify the registry{AnsiColors.RESET}"
            )
            err_count += 1

        # check: latest tag by default
        tag = img_parts[-1].split(":")[-1] if img_parts[-1].find(":") > 0 else "latest"
        if tag != "latest":
            print(
                f"  {AnsiColors.YELLOW}⚠ <{tbc_var.name}/{expected_input_name}>: container images should use 'latest' tag by default ('{tag}' found){AnsiColors.RESET}"
            )
            warn_count += 1

    # check variable declaration from doc (warn only)
    # -----------------------------------
    doc_var = next(filter(lambda dv: dv.var_name == tbc_var.name, doc_vars), None)
    if doc_var is None:
        print(
            f"  {AnsiColors.YELLOW}⚠ <{tbc_var.name}>: not documented in README{AnsiColors.RESET}"
        )
    else:
        # check default
        if doc_var.default(expected_gl_input.type) != expected_gl_input.default:
            print(
                f"  {AnsiColors.YELLOW}⚠ <{tbc_var.name}/{expected_input_name}>: README default ({doc_var.default(tbc_var.type)}) doesn't match Kicker's ({expected_gl_input.default}){AnsiColors.RESET}"
            )
            warn_count += 1

        if doc_var.lock and not tbc_var.secret:
            print(
                f"  {AnsiColors.YELLOW}⚠ <{tbc_var.name}>: is not declared as a secret but has a lock in README{AnsiColors.RESET}"
            )
            warn_count += 1
        elif not doc_var.lock and tbc_var.secret:
            print(
                f"  {AnsiColors.YELLOW}⚠ <{tbc_var.name}>: is declared as a secret but has no lock in README{AnsiColors.RESET}"
            )
            warn_count += 1
        elif not has_no_input and expected_input_name != doc_var.input_name:
            print(
                f"  {AnsiColors.YELLOW}⚠ <{tbc_var.name}/{expected_input_name}>: has wrong input declared in README ({doc_var.input_name}){AnsiColors.RESET}"
            )
            warn_count += 1

    # retrieve declared input from template specs
    declared_input = tpl_spec["spec"]["inputs"].get(expected_input_name)

    # check cases where variable must not be declared as an input
    # -----------------------------------------------------------
    if tbc_var.secret:
        # secrets should not be inputs
        if declared_input:
            print(
                f"  {AnsiColors.RED}✕ <{tbc_var.name}> is a secret: must not be declared{AnsiColors.RESET}"
            )
            return err_count + 1
        print(
            f"  {AnsiColors.HGRAY}✕ <{tbc_var.name}> is a secret: skip{AnsiColors.RESET}"
        )
        return err_count
    if root_kicker and _get_var(root_kicker, tbc_var.name):
        # a variant is overriding a variable from the main template: skip
        if declared_input:
            print(
                f"  {AnsiColors.RED}✕ <{tbc_var.name}> is an override: must not be declared{AnsiColors.RESET}"
            )
            return err_count + 1
        print(
            f"  {AnsiColors.HGRAY}✕ <{tbc_var.name}> is an override: skip{AnsiColors.RESET}"
        )
        return err_count
    if tbc_var.name.startswith("TBC_"):
        # global TBC variable: skip
        if declared_input:
            print(
                f"  {AnsiColors.RED}✕ <{tbc_var.name}> is global TBC: must not be declared{AnsiColors.RESET}"
            )
            return err_count + 1
        print(
            f"  {AnsiColors.HGRAY}✕ <{tbc_var.name}> is global TBC: skip{AnsiColors.RESET}"
        )
        return err_count

    # check if mapped GitLab input is declared
    if not declared_input:
        print(
            f"  {AnsiColors.RED}✕ <{tbc_var.name}/{expected_input_name}>: input not found{AnsiColors.RESET}"
        )
        return err_count + 1

    # check GitLab input is as expected
    # ---------------------------------
    actual_gl_input = GlInput.model_validate(declared_input)

    if actual_gl_input.type != expected_gl_input.type:
        print(
            f"  {AnsiColors.RED}✕ <{tbc_var.name}/{expected_input_name}>: type ({actual_gl_input.type}) doesn't match Kicker's ({expected_gl_input.type}){AnsiColors.RESET}"
        )
        err_count += 1

    if actual_gl_input.description != expected_gl_input.description:
        print(
            f"  {AnsiColors.YELLOW}⚠ <{tbc_var.name}/{expected_input_name}>: description doesn't match Kicker's{AnsiColors.RESET}"
        )
        warn_count += 1

    if actual_gl_input.default != expected_gl_input.default:
        print(
            f"  {AnsiColors.RED}✕ <{tbc_var.name}/{expected_input_name}>: default ({actual_gl_input.default}) doesn't match Kicker's ({expected_gl_input.default}){AnsiColors.RESET}"
        )
        err_count += 1

    if actual_gl_input.options != expected_gl_input.options:
        print(
            f"  {AnsiColors.RED}✕ <{tbc_var.name}/{expected_input_name}>: options ({actual_gl_input.options}) doesn't match Kicker's ({expected_gl_input.options}){AnsiColors.RESET}"
        )
        err_count += 1

    # check variable is initialized from input
    # ----------------------------------------
    actual_variable_value = tpl_body["variables"].get(tbc_var.name)
    expected_variable_value = f"$[[ inputs.{expected_input_name} ]]"
    if not actual_variable_value:
        print(
            f"  {AnsiColors.RED}✕ <{tbc_var.name}/{expected_input_name}>: variable not declared{AnsiColors.RESET}"
        )
        err_count += 1
    elif actual_variable_value != expected_variable_value:
        print(
            f"  {AnsiColors.RED}✕ <{tbc_var.name}/{expected_input_name}>: value ({actual_variable_value}) doesn't match Kicker's ({expected_variable_value}){AnsiColors.RESET}"
        )
        err_count += 1

    # print an OK message if no error was found
    if err_count == 0 and warn_count == 0:
        print(
            f"  {AnsiColors.GREEN}✓{AnsiColors.RESET} <{tbc_var.name}/{expected_input_name}>: OK"
        )

    return err_count


report_types_constraints = {
    "junit": {"formats": ["junit", "xunit"], "extensions": ["xml"]},
    "coverage_report": {"formats": ["cobertura"], "extensions": ["xml"]},
    "cyclonedx": {"formats": ["cyclonedx"], "extensions": ["json"]},
    "codequality": {"formats": ["codeclimate", "gitlab"], "extensions": ["json"]},
    "container_scanning": {"formats": ["gitlab"], "extensions": ["json"]},
    "load_performance": {"formats": ["summary"], "extensions": ["json"]},
}


def _check_report(
    job_name: str,
    type: str,
    path: str,
) -> int:
    """Check a report file."""
    if type in ["dotenv"]:
        # ignore
        return 0

    err_count = 0
    constraints = report_types_constraints.get(type)
    expected_formats = constraints["formats"] if constraints else None
    expected_exts = constraints["extensions"] if constraints else None
    if constraints is None:
        print(
            f"  {AnsiColors.HGRAY}? report '{path}' ({type}): unsupported report type{AnsiColors.RESET}"
        )

    path_parts = path.split("/")
    filename = path_parts[-1]
    filename_parts = filename.split(".")
    # check: first part shoud be job name
    if not filename_parts[0].startswith(job_name) or (
        len(filename_parts[0]) > len(job_name)
        and filename_parts[0][len(job_name)] != "-"
    ):
        print(
            f"  {AnsiColors.YELLOW}⚠ report '{path}' ({type}): filename (no extension) should preferably start with '{job_name}'{AnsiColors.RESET}"
        )
    # check: filename made of three parts
    if len(filename_parts) != 3:
        print(
            f"  {AnsiColors.YELLOW}⚠ report '{path}' ({type}): filename should preferably be in 3 parts '<job-name>.<format>.<extension>'{AnsiColors.RESET}"
        )
    else:
        # check: second part (format) matches report type
        if expected_formats and filename_parts[1] not in expected_formats:
            print(
                f"  {AnsiColors.RED}✕ report '{path}' ({type}): filename middle part must match '{'|'.join(expected_formats)}'{AnsiColors.RESET}"
            )
            err_count += 1

    # check: extension matches report type
    if expected_exts and filename_parts[-1] not in expected_exts:
        print(
            f"  {AnsiColors.RED}✕ report '{path}' ({type}): file extension must match '{'|'.join(expected_exts)}'{AnsiColors.RESET}"
        )
        err_count += 1

    # check: containing folder is 'reports/'
    if len(path_parts) < 2 or path_parts[-2] != "reports":
        print(
            f"  {AnsiColors.YELLOW}⚠ report '{path}' ({type}): should preferably be generated in a 'reports/' folder{AnsiColors.RESET}"
        )

    return err_count


def _check_job(job_name: str, tpl_body, job_prefix) -> int:
    err_count = 0

    # check non-hiden jobs rules
    if not job_name.startswith(".") and job_name != "pages":
        # check: all jobs are prefixed with the template prefix
        if not job_name.startswith(job_prefix) or (
            len(job_name) > len(job_prefix) and job_name[len(job_prefix)] != "-"
        ):
            print(
                f"  {AnsiColors.RED}✕ job <{job_name}>: doesn't start with prefix ({job_prefix}){AnsiColors.RESET}"
            )
            err_count += 1

    # check: tbc reports are compliant to naming convention
    reports = tpl_body[job_name].get("artifacts", {}).get("reports", {})
    for type, paths in reports.items():
        if type == "coverage_report":
            paths = paths["path"]
        if isinstance(paths, str):
            paths = [paths]
        for path in paths:
            err_count += _check_report(job_name, type, path)

    return err_count


def _check_inheritance(tpl_body: dict[str, Any]) -> int:
    err_count = 0

    # Gather actual jobs by filtering non-job items in template
    job_bodies = { name: body for name, body in tpl_body.items()
        if name not in ["stages", "workflow", "variables", "rules"]
           and isinstance(body, dict)
    }

    # Count non-hidden jobs to infer if it's a single job template or not
    is_single_job_tpl = sum(1 for name in job_bodies
               if not name.startswith(".")) == 1

    #
    # Each template
    #  - should define only one job
    # Or
    #  - all jobs should inherit from a hidden base job
    #
    for name, body in iter(job_bodies.items()):
        job_err_count = 0
        job_warn_count = 0
        # skip job with only partial overrides
        if not any(key in body for key in ["stage", "extends", "image"]):
            print(
                f"  {AnsiColors.HGRAY}✕ job '{name}' does not defines an actual job: skip{AnsiColors.RESET}"
            )
            continue
        base_job = body.get("extends", None)
        if is_single_job_tpl and base_job is not None:
            job_warn_count += 1
            print(
                f"  {AnsiColors.YELLOW}⚠ single job template: job '{name}' inherits another job{AnsiColors.RESET}"
            )
        if not is_single_job_tpl and not name.startswith(".") and base_job is None:
            job_err_count += 1
            print(
                f"  {AnsiColors.RED}✕ multiple job template: non-hidden job '{name}' should inherit a base job{AnsiColors.RESET}"
            )
        if base_job is not None and not base_job.startswith("."):
            job_err_count += 1
            print(
                f"  {AnsiColors.RED}✕ job '{name}' inherits from non-hidden job '{base_job}'{AnsiColors.RESET}"
            )
        if base_job is not None and base_job not in job_bodies:
            job_warn_count += 1
            print(
                f"  {AnsiColors.YELLOW}⚠ job '{name}' inherits from '{base_job}' which is not defined in current template{AnsiColors.RESET}"
            )
        if job_err_count == 0 and job_warn_count == 0:
            print(
                f"  {AnsiColors.GREEN}✓{AnsiColors.RESET} job '{name}' inheritance: OK"
            )
        err_count += job_err_count

    return err_count

def _check_tpl(
    kicker: dict[str, any],
    root_kicker: Optional[dict[str, any]],
    project_dir: Path,
    prefix: str,
    job_prefix: str,
    doc_vars: list[DocVar],
) -> int:
    """Check a template (either main or variant)."""
    tpl_path = project_dir / kicker["template_path"]
    if not tpl_path.exists():
        print(f"{AnsiColors.RED}ERROR: Template file ({tpl_path}) not found: abort")
        exit(1)

    # load template
    with open(tpl_path, "r") as reader:
        tpl_parts: list[dict[str, Any]] = list(
            yaml.load_all(reader, Loader=yaml.BaseLoader)
        )
        tpl_spec: dict[str, Any] = tpl_parts[0]
        tpl_body: dict[str, Any] = tpl_parts[-1]

    inputs: dict[str, dict[str, any]] = dict(tpl_spec["spec"]["inputs"])
    err_count = 0

    # check inheritance
    # -----------------
    err_count += _check_inheritance(tpl_body=tpl_body)

    # check jobs
    # ----------
    for name, body in tpl_body.items():
        if "stage" not in body and "extends" not in body:
            # not a job?,
            continue
        err_count += _check_job(name, tpl_body, job_prefix)

    # check variables
    # ---------------
    var_prefix = prefix.upper() + "_"

    # check main variables
    for var in kicker.get("variables", []):
        tbc_var = TbcVar.model_validate(var)
        err_count += _check_var(
            tbc_var, var_prefix, tpl_spec, tpl_body, root_kicker, doc_vars
        )
        input_name = tbc_var.input_name(var_prefix)
        if input_name in inputs:
            del inputs[input_name]

    # check feature variables
    for feat in kicker.get("features", []):
        if feat.get("enable_with"):
            tbc_var = TbcVar(
                name=feat.get("enable_with"),
                description=f"Enable {feat['name']}",
                type=TbcVarType.boolean,
            )
            err_count += _check_var(
                tbc_var, var_prefix, tpl_spec, tpl_body, root_kicker, doc_vars
            )
            input_name = tbc_var.input_name(var_prefix)
            if input_name in inputs:
                del inputs[input_name]
        elif feat.get("disable_with"):
            tbc_var = TbcVar(
                name=feat.get("disable_with"),
                description=f"Disable {feat['name']}",
                type=TbcVarType.boolean,
            )
            err_count += _check_var(
                tbc_var, var_prefix, tpl_spec, tpl_body, root_kicker, doc_vars
            )
            input_name = tbc_var.input_name(var_prefix)
            if input_name in inputs:
                del inputs[input_name]
        for var in feat.get("variables", []):
            tbc_var = TbcVar.model_validate(var)
            err_count += _check_var(
                tbc_var, var_prefix, tpl_spec, tpl_body, root_kicker, doc_vars
            )
            input_name = tbc_var.input_name(var_prefix)
            if input_name in inputs:
                del inputs[input_name]

    # remaining inputs are unmapped inputs
    for input_name in inputs:
        print(
            f"  {AnsiColors.RED}✕ unmapped declared input '{input_name}'{AnsiColors.RESET}"
        )

    err_count += len(inputs)
    return err_count


def _load_vars_from_doc(project_dir: Path) -> list[DocVar]:
    # read file
    readme_path = project_dir / "README.md"
    if not readme_path.exists():
        print(f"{AnsiColors.RED}ERROR: Readme file ({readme_path}) not found: abort")
        exit(1)
    with open(readme_path, "r") as reader:
        readme_content = reader.read()

    # group 1: ':lock:'
    # group 2: <name>
    # group 3 (optional): <name>
    # group 4: <description>
    # group 5: <default>
    var_doc_regex = re.compile(
        r"^\| *(:lock:)? *`([a-zA-Z0-9-_]+)`(?: *\/ *`([a-zA-Z0-9-_]+)`)? *\| *([^|]*) *\| *([^|]*)\ *\|$",
        flags=re.MULTILINE,
    )
    return [
        DocVar(
            lock=bool(match.group(1)),
            var_name=match.group(3) or match.group(2),
            input_name=match.group(2) if match.group(3) else None,
            description=match.group(4),
            default_cell=match.group(5),
        )
        for match in var_doc_regex.finditer(readme_content)
    ]


def run():
    # define command parser
    parser = argparse.ArgumentParser(
        prog="tbc-check",
        description="This tool performs several checks on a to-be-continuous template project",
    )
    parser.add_argument("project_dir", default=".")

    # parse command and args
    args = parser.parse_args()

    project_dir = Path(args.project_dir)
    if not project_dir.exists():
        print(
            f"{AnsiColors.RED}ERROR: Project path ({project_dir}) does not exist: abort"
        )
        exit(1)
    if not project_dir.is_dir():
        print(
            f"{AnsiColors.RED}ERROR: Project path ({project_dir}) is not a directory: abort"
        )
        exit(1)

    # load kicker file
    kicker_path = project_dir / "kicker.json"
    if not kicker_path.exists():
        print(f"{AnsiColors.RED}ERROR: Kicker file ({kicker_path}) not found: abort")
        exit(1)

    with open(kicker_path, "r") as reader:
        kicker = json.load(reader)

    # load vars from README
    doc_vars = _load_vars_from_doc(project_dir)

    # retrieve prefix from kicker or guess it from first XXX_IMAGE variable
    prefix: str = (
        kicker.get("prefix")
        or next(
            map(
                lambda var: var["name"].split("_")[0],
                filter(
                    lambda var: var["name"].endswith("_IMAGE"),
                    kicker.get("variables", []),
                ),
            ),
            None,
        )
        or kicker["name"].lower()
    )
    job_prefix: str = kicker.get("job_prefix", prefix)

    print("=============================================================")
    print(
        f"Checking template {AnsiColors.CYAN}{kicker['name']}{AnsiColors.RESET} (prefix {AnsiColors.CYAN}\"{prefix}\"{AnsiColors.RESET} / job prefix {AnsiColors.CYAN}\"{job_prefix}\"{AnsiColors.RESET})"
    )
    print("=============================================================")
    if not kicker.get("is_component"):
        print(
            f"{AnsiColors.HGRAY}✕ this project is not a CI/CD component: skip{AnsiColors.RESET}"
        )
        return

    # Check main template
    print(
        f"{AnsiColors.BLUE}{AnsiColors.BOLD}→ Main template ({kicker['template_path']}){AnsiColors.RESET}"
    )
    err_count = _check_tpl(kicker, None, project_dir, prefix, job_prefix, doc_vars)

    # Check variants
    for variant in kicker.get("variants", []):
        print(
            f"{AnsiColors.BLUE}{AnsiColors.BOLD}→ {variant['name']} variant ({variant['template_path']}){AnsiColors.RESET}"
        )
        err_count += _check_tpl(
            variant, kicker, project_dir, prefix, job_prefix, doc_vars
        )

    if err_count > 0:
        exit(127)
