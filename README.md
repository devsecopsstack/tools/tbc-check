# tbc-check CLI tool

`tbc-check` is a basic CLI program that checks to-be-continuous templates.

## Install

```bash
pip install tbc-check --index-url https://gitlab.com/api/v4/projects/54325774/packages/pypi/simple --upgrade

# obtain help
tbc-check --help
```

## Checked rules

`tbc-check` checks the following rules:

- the existence of the `kicker.json` file
- the existence of the `README.md` file
- for each variable in the `kicker.json` file (main template and variants):
    - emtpy default values should be omitted
    - mapped input is declared in the related template with same name, type, default, options and description (WARNING only)
    - the variable is declared in the related template with expected input mapping (`$[[ inputs.input-name ]]`)
    - the input/variable is documented in the README with same default (WARNING only)
    - for variables corresponding to container images:
        - [explicit image registry](https://to-be-continuous.gitlab.io/doc/dev/architecture/#explicit-official-image-registry) is specified
        - uses [latest tag by default](https://to-be-continuous.gitlab.io/doc/dev/architecture/#latest-version-by-default)
- for each job defined in GitLab templates:
    - the job starts with the [template (job) prefix](https://to-be-continuous.gitlab.io/doc/dev/guidelines/#prefix-job-names)
    - each report file complies to the [naming conventions](https://to-be-continuous.gitlab.io/doc/dev/architecture/#report-files-naming-convention)


If any non-WARNING rule fails, `tbc-check` fails with exit code 127.

## Developers

```bash
# install dependencies
poetry install

# obtain help
poetry run tbc-check --help

# run tool
poetry run tbc-check path/to/tbc/template
```
